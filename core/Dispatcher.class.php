<?php

namespace core;

/**
 * Description of Dispatcher
 *
 * @author ronaldo.silva
 */
class Dispatcher{

    protected static $instance;
    private $controller;
    private $requestUri;
    private $defaultController = 'index';
    private $controllerNotFound = 'page-not-found';

    /**
     *
     * @return Dispatcher A instancia atual de Dispatcher
     */
    public static function getInstance(){
        if(!Dispatcher::$instance){
            Dispatcher::$instance = new Dispatcher();
        }
        return self::$instance;
    }

    /**
     * Captura o controller passado pela url
     *
     * @return void
     */
    public function dispatch(){
        $this->getController();

        if(!$this->controller){
            $this->controller = $this->defaultController;
        }

        if(!class_exists('\controller\\' . \core\lib\Tools::toggleCamelCase($this->controller, false))){
            $this->controller = $this->controllerNotFound;
        }
        $controllerName = \core\lib\Tools::toggleCamelCase($this->controller, false);
        $controller = \controller\Controller::getController('\controller\\' . $controllerName);
        $this->controller = '';

        $controller->run();
    }

    public function getController(){

        if($this->controller){
            $_GET['controller'] = $this->controller;
            return $this->controller;
        }

        $controller = \core\lib\Tools::getValue('controller');



//        if(!\app\lib\Validate::isControllerName($controller)){
//            $controller = false;
//        }





        if(!$this->requestUri){
            return $this->controllerNotFound;
        }

        $controller = $this->controllerNotFound;


        if(!preg_match('/\.(gif|jpe?g|png|css|js|ico)$/i', $this->requestUri)){

            if(isset($this->routes)){

                foreach($this->routes as $route){

                    if($this->matches($route['rule'])){

                        $controller = isset($route['controller']) ? $route['controller'] : $_GET['controller'];
                    }
                }
            }
        }


        $this->controller = $controller;

        $_GET['controller'] = $this->controller;

        return $this->controller;
    }

    /**
     * Separa a uri da url
     *
     * @return void
     */
    private function setRequestUri(){
        $scriptName = $_SERVER['SCRIPT_NAME']; // <-- "/foo/index.php"
        $requestUri = $_SERVER['REQUEST_URI']; // <-- "/foo/bar?test=abc" or "/foo/index.php/bar?test=abc"
        $queryString = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : ''; // <-- "test=abc" or ""

        if(strpos($requestUri, $scriptName) !== false){
            $physicalPath = $scriptName; // <-- Without rewriting
        }else{
            $physicalPath = str_replace('\\', '', dirname($scriptName)); // <-- With rewriting
        }

        $env['SCRIPT_NAME'] = rtrim($physicalPath, '/'); // <-- Remove trailing slashes

        $this->requestUri = rawurldecode($this->requestUri);
        $env['PATH_INFO'] = substr_replace($requestUri, '', 0, strlen($physicalPath)); // <-- Remove physical path
        $env['PATH_INFO'] = str_replace('?' . $queryString, '', $env['PATH_INFO']); // <-- Remove query string
        $this->requestUri = '/' . trim($env['PATH_INFO'], '/'); // <-- Ensure leading slash
    }

}
