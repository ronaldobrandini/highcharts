<?php 
error_reporting(E_ALL); 
require_once '/core/config/cfg.php';

core\Dispatcher::getInstance()->dispatch();


die();
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/solarized_light.css">

        <script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.highcharts.com/highcharts.js"></script>
        <script src="//code.highcharts.com/modules/exporting.js"></script>
        <script src="js/highlight.pack.js"></script>
        <script>hljs.initHighlightingOnLoad();</script>
    </head>
    <body>
        <?php include_once 'header.php'; ?>
        <section>
            <menu class="col-lg-4">
                <ul class="nav nav-pills nav-stacked">
                    <h2>Line</h2>
                    <li>
                        <a href="?example=line-charts/basic-line">Basic line</a>
                    </li>
                    <li>
                        <a href="?example=line-charts/data-label">With data label</a>
                    </li>
                    <li>
                        <a href="?example=line-charts/time-series">Time series, zoomable </a>
                    </li>
                    <h2>Column and bar charts</h2>
                    <li>
                        <a href="?example=column-charts/basic-bar">Basic Bar </a>
                    </li>
                </ul>
            </menu>
            <article class="col-lg-8" id="content">
                <?php
                
                if(isset($_GET['example'])){

                    if(file_exists('example/' . $_GET['example'] . '.php')){
                        include_once 'example/' . $_GET['example'] . '.php';
                    }else{
                        echo 'eita';
                    }
                }else{
                    include_once 'example/line-charts/basic-line.php';
                }
                ?>
            </article>
        </section>
    </body>
</html>