<div class="jumbotron">
    <h1>Highcharts PHP!</h1>
    <p>
        Crie graficos <a href="//www.highcharts.com">Highcharts</a> integrados
        com o php de forma simples digitando apenas códigos PHP.
    </p>
    <p><a class="btn btn-primary btn-lg" role="button">Download</a></p>
</div>
<div class="container-fluid">
    <p>
        Todos nós precisamos criar graficos em nossas aplicações, existem 
        diversas soluções disponiveis para esse fim como 
        <a href="//developers.google.com/chart/?hl=pt-br">Google Chart</a>,
        <a href="//www.chartjs.org/">Chart JS</a> e 
        <a href="//www.highcharts.com">Highcharts</a> que são desenvolvidos em 
        javascript garantindo uma boa interatividade com o usuário. Por outro
        lado existe também formas de desenvolver graficos utilizando o 
        <strong>PHP</strong> porém perdemos a interatividade. 
    </p>
    <p>
        Após escolher pela interatividade temos o problema para fazer a
        integração javascrip/php e muitas vezes o código acaba ficando confuso.
        Veja abaixo um exemplo de código ruim.
    </p>
    <h3>Ex:</h3>
    <div class="col-lg-6">
        <pre>
            <code>
&LT;script&gt;
    $(function () {
        var chart;
        $(document).ready(function() {
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'container_parceria',
                    type: 'column'
                },
                title: {
                    text: 'Parceria'
                },
                xAxis: {
                    categories: [ &lt;?php
                        foreach($arrayParcerias AS $valor){
                            $categories. = "'".$valor."',";
                        }
                        echo substr($categories, 0, - 1);
                    ?&gt; 
                    ],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Percentual parcerias',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    formatter: function() {
                        return '' +
                        this.series.name + ': ' + this.y + '%';
                    }
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: - 100,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: '#FFFFFF',
                shadow: true
            },
            series: [{
                name: 'Sim',
                data: [ &lt;?php
                    for ($i = 1; $i <= 11; $i++){
                        $total[$i] = $respostaPositiva[$i] + $respostaNegativa[$i];
                        echo (!empty($respostaPositiva[$i])) ? round((($respostaPositiva[$i] / $total[$i]) * 100), 1) : 0;
                        echo ($i != 11) ? ',' : '';
                    }
                ?> ]
                }, {
                    name: '&lt;?= 'Nao'; ?&gt;',
                    data: [ &lt;?php
                            for ($i = 1; $i <= 11; $i++){
                                $total[$i] = $respostaPositiva[$i] + $respostaNegativa[$i];
                                echo (!empty($respostaNegativa[$i])) ? round((($respostaNegativa[$i] / $total[$i]) * 100), 1) : 0;
                                echo ($i != 11) ? ',' : '';
                            }
                    ?&gt;]
                }]
            });    
        });
    });
&lt;/script&gt;
            </code>
        </pre>
    </div>
    <div class="col-lg-6">
        <p>
            Logo a primeira vista já notamos que o código possui uma dificuldade
            a mais para depuração de erros e levara algum tempo para entender o 
            seu funcionamento.
        </p>
        <p>
            A fim de deixar essa integração um pouco mais intuitiva desenvolvi 
            uma ferramenta para gerar codigos Highcharts a partir do <abbr>PHP</abbr>. 
            <br>
            Veja abaixo um exemplo de implementação básica.
        </p>
        <pre>
            <code>
//Opções do highcharts
/*
 * Estancia o chart definindo o elemento e o tipo de chart
 */
$chart = new \core\tools\highchart\Chart('container', 'line');

/*
 * Estancia e define o Titulo
 */
$title = new \core\tools\highchart\Title('title');
$title->setDefinitions('text', 'Monthly Average Temperature');
$title->setDefinitions('x', '-20');

/*
 * Estancia o título novamente passando subtitle como parametro
 */
$subtitle = new \core\tools\highchart\Title('subtitle');
$subtitle->setDefinitions('text', 'Source: WorldClimate.com');
$subtitle->setDefinitions('x', '-20');

/*
 * Estancia e define o Xasis
 */
$xAxis = new \core\tools\highchart\XAxis();
$xAxis->setDefinitions('categories', array(
    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
        )
);

/*
 * Estancia e define o Yaxis
 */
$yAxis = new \core\tools\highchart\YAxis();
$yAxis->setDefinitions('title', array('text' => 'Temperature (°C)'));
$yAxis->setDefinitions('plotLines', array(array('value' => 0, 'width' => 1, 'color' => '#808080')));

/*
 * Define o tooltip
 */
$tooltip = new \core\tools\highchart\Tooltip();
$tooltip->setDefinitions('valueSuffix', 'ºC');
/*
 * Define a legenda
 */
$legend = new \core\tools\highchart\Legend();
$legend->setDefinitions('layout', 'vertical');
$legend->setDefinitions('align', 'right');
$legend->setDefinitions('verticalAlign', 'middle');
$legend->setDefinitions('borderWidth', 0);

/*
 * Definindo as series
 */
$serie1 = new \core\tools\highchart\Series();
$serie1->setDefinitions('data', array(7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6));
$serie1->setDefinitions('name', 'Tokyo');

$serie2 = new \core\tools\highchart\Series();
$serie2->setDefinitions('data', array(-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5));
$serie2->setDefinitions('name', 'New York');

$serie3 = new \core\tools\highchart\Series();
$serie3->setDefinitions('data', array(-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0));
$serie3->setDefinitions('name', 'Berlin');

$serie4 = new \core\tools\highchart\Series();
$serie4->setDefinitions('data', array(3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8));
$serie4->setDefinitions('name', 'London');

// Montando as Opções

$highcharts = new \core\tools\highchart\Highchart($chart);
$highcharts->setTitle($title);
$highcharts->setSubTitle($subtitle);
$highcharts->setXAxis($xAxis);
$highcharts->setYAxis($yAxis);
$highcharts->setTooltip($tooltip);
$highcharts->setLegend($legend);
$highcharts->setSeries($serie1);
$highcharts->setSeries($serie2);
$highcharts->setSeries($serie3);
$highcharts->setSeries($serie4);
echo $highcharts->render();
            </code>
        </pre>
    </div>

</div>