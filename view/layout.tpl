<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <!-- BEGIN block_css -->
        <link rel="stylesheet" href="{css}" media="{media}">
        <!-- END block_css -->
    </head>
    <body>
        <header>{header}</header>
        {content}
        {footer}
        <!-- BEGIN block_js -->
        <script src="{js}" type="text/javascript"></script>
        <!-- END block_js -->
        <!-- BEGIN block_js_string -->
        <script type="text/javascript">{jsString}</script>
        <!-- END block_js_string -->
    </body>
</html>