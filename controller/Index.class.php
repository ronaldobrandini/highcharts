<?php

namespace controller;

/**
 * Description of Index
 *
 * @author ronaldo.silva
 */
class Index extends Controller{

    public function initContent(){
        $this->tpl->addFile('content', 'view/index.tpl');
    }

    protected function setMedia(){
        parent::setMedia();
        $this->addCSS('solarized_light.css');
        $this->addJs('highlight.pack.js');
        $this->jsString[] = 'hljs.initHighlightingOnLoad();';
    }

}

