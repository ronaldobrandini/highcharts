<?php
namespace controller;
abstract class Controller{
    protected $tpl;
    private $cssFiles = array();
    private $jsFiles = array();
    protected $jsString = array();
    
    public function __construct(){
        $this->tpl = new \core\lib\Template('view/layout.tpl');
        $this->setMedia();
    }
    
    public abstract function initContent();


    public static function getController($controllerName){
        return new $controllerName();
    }
    
    private function initHeader(){
        $this->tpl->addFile('header', 'view/header.tpl');
        
    }
    
    /**
     * Adiciona um novo Arquivo CSS na página
     * @see \core\lib\Media::getCSSPath()
     * 
     * @param mixed $cssUri Caminho para o arquivo ou uma lista no formato array(array( uri => mediaType ), ...))
     * @param string $mediaType MediaType do arquivo (all, print, screen) Padrão all
     * @return bool 
     */
    public function addCSS($cssUri, $cssMediaType = 'all'){
        $media = new \core\lib\Media();
        if(is_array($cssUri)){
            foreach($cssUri as $cssFile => $media){
                if(is_string($cssFile) && strlen($cssFile) > 1){
                    $cssPath = $media->getCSSPath($cssFile, $media);
                    if($cssPath && !in_array($cssPath, $this->cssFiles)){
                        $this->cssFiles = array_merge($this->cssFiles, $cssPath);
                    }
                }else{
                    $cssPath = $media->getCSSPath($media, $cssMediaType);
                    if($cssPath && !in_array($cssPath, $this->cssFiles)){
                        $this->cssFiles = array_merge($this->cssFiles, $cssPath);
                    }
                }
            }
        }else if(is_string($cssUri) && strlen($cssUri) > 1){
            $cssPath = $media->getCSSPath($cssUri, $cssMediaType);
            if($cssPath){
                $this->cssFiles = array_merge($this->cssFiles, $cssPath);
            }
        }
    }

    /**
     * Add um novo arquivo js na página
     * @see \core\lib\Media::getJSPath()
     * @param mixed $jsUri
     * @return void
     */
    public function addJs($jsUri){
        $media = new \core\lib\Media();
        if(is_array($jsUri)){
            foreach($jsUri as $jsFile){
                $jsPath = $media->getJSPath($jsFile);
                if($jsPath && !in_array($jsPath, $this->jsFiles)){
                    $this->jsFiles[] = $jsPath;
                }
            }
        }else{
            $jsPath = $media->getJSPath($jsUri);
            if($jsPath){
                $this->jsFiles[] = $jsPath;
            }
        }
    }

    private function show(){
        echo $this->tpl->parse();
    }


    public function run(){
        $this->initHeader();
        $this->initContent();
        
        if($this->cssFiles){
            foreach($this->cssFiles as $css => $media){
                $this->tpl->css = $css;
                $this->tpl->media = $media;
                $this->tpl->block('block_css');
            }
        }
        
        if($this->jsFiles){
            foreach($this->jsFiles as $js){
                $this->tpl->js = $js;
                $this->tpl->block('block_js');
            }
        }
        
        if($this->jsString){
            foreach($this->jsString as $jsString){
                $this->tpl->jsString = $jsString;
                $this->tpl->block('block_js_string');
            }
        }
        $this->show();
    }
    
    protected function setMedia(){
        $this->addCSS('bootstrap.min.css');
        $this->addCSS('style.css');
    }
}
