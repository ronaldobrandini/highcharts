<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<?php
//Opções do highcharts
/*
 * Estancia o chart definindo o elemento e o tipo de chart
 */
$chart = new \core\tools\highchart\Chart('container', 'line');

/*
 * Estancia e define o Titulo
 */
$title = new \core\tools\highchart\Title('title');
$title->setDefinitions('text', 'Monthly Average Temperature');

/*
 * Estancia o título novamente passando subtitle como parametro
 */
$subtitle = new \core\tools\highchart\Title('subtitle');
$subtitle->setDefinitions('text', 'Source: WorldClimate.com');

/*
 * Estancia e define o Xasis
 */
$xAxis = new \core\tools\highchart\XAxis();
$xAxis->setDefinitions('categories', array(
    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
        )
);

/*
 * Estancia e define o Yaxis
 */
$yAxis = new \core\tools\highchart\YAxis();
$yAxis->setDefinitions('title', array('text' => 'Temperature (°C)'));

/*
 * Define o tooltip
 */
$tooltip = new \core\tools\highchart\Tooltip();
$tooltip->setDefinitions('valueSuffix', 'ºC');
/*
 * Definindo o plotOptions
 */
$plotOptions = new \core\tools\highchart\plotOptions\Line();
$plotOptions->setDefinitions('dataLabels', array('enabled' => true));
$plotOptions->setDefinitions('enableMouseTracking', false);

/*
 * Definindo as series
 */
$serie1 = new \core\tools\highchart\Series();
$serie1->setDefinitions('data', array(7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6));
$serie1->setDefinitions('name', 'Tokyo');

$serie2 = new \core\tools\highchart\Series();
$serie2->setDefinitions('data', array(3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8));
$serie2->setDefinitions('name', 'London');

// Montando as Opções

$highcharts = new \core\tools\highchart\Highchart($chart);
$highcharts->setTitle($title);
$highcharts->setSubTitle($subtitle);
$highcharts->setXAxis($xAxis);
$highcharts->setYAxis($yAxis);
$highcharts->setTooltip($tooltip);
$highcharts->setPlotOptions($plotOptions);
$highcharts->setSeries($serie1);
$highcharts->setSeries($serie2);
$js = $highcharts->render();

echo '<div class="col-lg-6"><div class="panel panel-info"><div class="panel-heading">Código Javascript gerado</div><div class="panel-body"><pre><code class="javascript">' . $js . '</code></pre></div></div></div>';
echo '<div class="col-lg-6"><div class="panel panel-info"><div class="panel-heading">Código PHP</div><div class="panel-body">';
?>
<pre>
<code>
//Opções do highcharts
/*
 * Estancia o chart definindo o elemento e o tipo de chart
 */
$chart = new \core\tools\highchart\Chart('container', 'line');

/*
 * Estancia e define o Titulo
 */
$title = new \core\tools\highchart\Title('title');
$title->setDefinitions('text', 'Monthly Average Temperature');

/*
 * Estancia o título novamente passando subtitle como parametro
 */
$subtitle = new \core\tools\highchart\Title('subtitle');
$subtitle->setDefinitions('text', 'Source: WorldClimate.com');

/*
 * Estancia e define o Xasis
 */
$xAxis = new \core\tools\highchart\XAxis();
$xAxis->setDefinitions('categories', array(
    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
        )
);

/*
 * Estancia e define o Yaxis
 */
$yAxis = new \core\tools\highchart\YAxis();
$yAxis->setDefinitions('title', array('text' => 'Temperature (°C)'));

/*
 * Define o tooltip
 */
$tooltip = new \core\tools\highchart\Tooltip();
$tooltip->setDefinitions('valueSuffix', 'ºC');
/*
 * Definindo o plotOptions
 */
$plotOptions = new \core\tools\highchart\plotOptions\Line();
$plotOptions->setDefinitions('dataLabels', array('enabled' => true));
$plotOptions->setDefinitions('enableMouseTracking', false);

/*
 * Definindo as series
 */
$serie1 = new \core\tools\highchart\Series();
$serie1->setDefinitions('data', array(7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6));
$serie1->setDefinitions('name', 'Tokyo');

$serie2 = new \core\tools\highchart\Series();
$serie2->setDefinitions('data', array(3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8));
$serie2->setDefinitions('name', 'London');

// Montando as Opções

$highcharts = new \core\tools\highchart\Highchart($chart);
$highcharts->setTitle($title);
$highcharts->setSubTitle($subtitle);
$highcharts->setXAxis($xAxis);
$highcharts->setYAxis($yAxis);
$highcharts->setTooltip($tooltip);
$highcharts->setPlotOptions($plotOptions);
$highcharts->setSeries($serie1);
$highcharts->setSeries($serie2);
echo $highcharts->render();
    
</code>
</pre>
<?php
echo '</div></div></div>';

echo '<script>' . $js . '</script>';



