<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<?php
//Opções do highcharts
/*
 * Estancia o chart definindo o elemento e o tipo de chart
 */
$chart = new \core\tools\highchart\Chart('container', 'bar');

/*
 * Estancia e define o Titulo
 */
$title = new \core\tools\highchart\Title('title');
$title->setDefinitions('text', 'Historic World Population by Region');

/*
 * Estancia o título novamente passando subtitle como parametro
 */
$subtitle = new \core\tools\highchart\Title('subtitle');
$subtitle->setDefinitions('text', 'Source: Wikipedia.org');

/*
 * Estancia e define o Xasis
 */
$xAxis = new \core\tools\highchart\XAxis();
$xAxis->setDefinitions('categories', array(
    'Africa', 'America', 'Asia', 'Europe', 'Oceania'
));

/*
 * Estancia e define o Yaxis
 */
$yAxis = new \core\tools\highchart\YAxis();
$yAxis->setDefinitions('title', array('text' => 'Population (millions)', 'align' => 'high'));
$yAxis->setDefinitions('labels', array('overflow' => 'justify'));

/*
 * Define o tooltip
 */
$tooltip = new \core\tools\highchart\Tooltip();
$tooltip->setDefinitions('valueSuffix', 'millions');
/*
 * Define a legenda
 */
$legend = new \core\tools\highchart\Legend();
$legend->setDefinitions('layout', 'vertical');
$legend->setDefinitions('align', 'right');
$legend->setDefinitions('verticalAlign', 'top');
$legend->setDefinitions('x', -40);
$legend->setDefinitions('y', 100);
$legend->setDefinitions('floating', true);
$legend->setDefinitions('shadow', true);
$legend->setDefinitions('shadow', "((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF')");

/*
 * Definindo o plotOptions
 */
$plotOptions = new \core\tools\highchart\plotOptions\Bar();
$plotOptions->setDefinitions('dataLabels', array('enabled' => true));

/*
 * Definindo as series
 */
$serie1 = new \core\tools\highchart\Series();
$serie1->setDefinitions('data', array(107, 31, 635, 203, 2));
$serie1->setDefinitions('name', 'Year 1800');

$serie2 = new \core\tools\highchart\Series();
$serie2->setDefinitions('data', array(133, 156, 947, 408, 6));
$serie2->setDefinitions('name', 'Year 1900');

$serie3 = new \core\tools\highchart\Series();
$serie3->setDefinitions('data', array(973, 914, 4054, 732, 34));
$serie3->setDefinitions('name', 'Year 2008');

/*
 * Definindo os Creditos
 */
$credits = new core\tools\highchart\Credits();
$credits->setDefinitions('enabled', false);

// Montando as Opções

$highcharts = new \core\tools\highchart\Highchart($chart);
$highcharts->setTitle($title);
$highcharts->setSubTitle($subtitle);
$highcharts->setXAxis($xAxis);
$highcharts->setYAxis($yAxis);
$highcharts->setTooltip($tooltip);
$highcharts->setLegend($legend);
$highcharts->setSeries($serie1);
$highcharts->setSeries($serie2);
$highcharts->setSeries($serie3);
$highcharts->setCredits($credits);
$js = $highcharts->render();

echo '<div class="col-lg-6"><div class="panel panel-info"><div class="panel-heading">Código Javascript gerado</div><div class="panel-body"><pre><code class="javascript">' . $js . '</code></pre></div></div></div>';
echo '<div class="col-lg-6"><div class="panel panel-info"><div class="panel-heading">Código PHP</div><div class="panel-body">';
?>
<pre>
<code>
//Opções do highcharts
/*
 * Estancia o chart definindo o elemento e o tipo de chart
 */
$chart = new \core\tools\highchart\Chart('container', 'bar');

/*
 * Estancia e define o Titulo
 */
$title = new \core\tools\highchart\Title('title');
$title->setDefinitions('text', 'Historic World Population by Region');

/*
 * Estancia o título novamente passando subtitle como parametro
 */
$subtitle = new \core\tools\highchart\Title('subtitle');
$subtitle->setDefinitions('text', 'Source: Wikipedia.org');

/*
 * Estancia e define o Xasis
 */
$xAxis = new \core\tools\highchart\XAxis();
$xAxis->setDefinitions('categories', array(
    'Africa', 'America', 'Asia', 'Europe', 'Oceania'
));

/*
 * Estancia e define o Yaxis
 */
$yAxis = new \core\tools\highchart\YAxis();
$yAxis->setDefinitions('title', array('text' => 'Population (millions)', 'align' => 'high'));
$yAxis->setDefinitions('labels', array('overflow' => 'justify'));

/*
 * Define o tooltip
 */
$tooltip = new \core\tools\highchart\Tooltip();
$tooltip->setDefinitions('valueSuffix', 'millions');
/*
 * Define a legenda
 */
$legend = new \core\tools\highchart\Legend();
$legend->setDefinitions('layout', 'vertical');
$legend->setDefinitions('align', 'right');
$legend->setDefinitions('verticalAlign', 'top');
$legend->setDefinitions('x', -40);
$legend->setDefinitions('y', 100);
$legend->setDefinitions('floating', true);
$legend->setDefinitions('shadow', true);
$legend->setDefinitions('shadow', "((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF')");

/*
 * Definindo o plotOptions
 */
$plotOptions = new \core\tools\highchart\plotOptions\Bar();
$plotOptions->setDefinitions('dataLabels', array('enabled' => true));

/*
 * Definindo as series
 */
$serie1 = new \core\tools\highchart\Series();
$serie1->setDefinitions('data', array(107, 31, 635, 203, 2));
$serie1->setDefinitions('name', 'Year 1800');

$serie2 = new \core\tools\highchart\Series();
$serie2->setDefinitions('data', array(133, 156, 947, 408, 6));
$serie2->setDefinitions('name', 'Year 1900');

$serie3 = new \core\tools\highchart\Series();
$serie3->setDefinitions('data', array(973, 914, 4054, 732, 34));
$serie3->setDefinitions('name', 'Year 2008');

/*
 * Definindo os Creditos
 */
$credits = new core\tools\highchart\Credits();
$credits->setDefinitions('enabled', false);

// Montando as Opções

$highcharts = new \core\tools\highchart\Highchart($chart);
$highcharts->setTitle($title);
$highcharts->setSubTitle($subtitle);
$highcharts->setXAxis($xAxis);
$highcharts->setYAxis($yAxis);
$highcharts->setTooltip($tooltip);
$highcharts->setLegend($legend);
$highcharts->setSeries($serie1);
$highcharts->setSeries($serie2);
$highcharts->setSeries($serie3);
$highcharts->setCredits($credits);
echo $highcharts->render();
    
</code>
</pre>
<?php
echo '</div></div></div>';

echo '<script>' . $js . '</script>';



